function getData() {
	chart = document.getElementById('chart');

	Plotly.plot( chart, [{
	    x: [1, 2, 3, 4, 5],
	    y: [1, 2, 4, 8, 16] }], { 
	    margin: { t: 0 } }, {showSendToCloud:true} );

	/* Current Plotly.js version */
	console.log( Plotly.BUILD );
}

// When a user uploads a new text file, the function gets called
// and updates the plot with the given data.
function onUpload() {
	let inputFile = document.getElementById("file-upload").files[0];
	console.log(inputFile.size);
	var fileReader = new FileReader();

	var resultText;

	fileReader.onload = function(e) {
		resultText = fileReader.result;  // getting the contents of the file.
		if (resultText.indexOf(',') > -1) {  // if the file type is csv, proceed with processing the data.
			
			console.log("File input valid.");

			csvDataSplit = resultText.split(",");

			// Getting the x-axis elements.
			// Could be done better with enumeration (Python).
			xAxis = new Array();
			for (var i = 0; i < csvDataSplit.length; i++) {
				console.log("Element["+i+"]: " + csvDataSplit[i]);
				xAxis.push(i+1);
			}

			// Getting the element to place the chart.
			chart = document.getElementById('chart');

			// Formatting the plot data.
			var input_data = {
				x: xAxis,
				y: csvDataSplit
			}

			var data = [input_data];

			// Formatting Layout.
			var title_string, xaxis_string, yaxis_string;
			xaxis_string = "Time (s)"
			if (inputFile.name.toLowerCase() == "press.txt") {
				title_string = "Pressure Data";
				yaxis_string = "Pressure (hPa)";
			} else if (inputFile.name.toLowerCase() == "humid.txt") {
				title_string = "Humidity Data";
				yaxis_string = "Humidity (%)";
			} else if (inputFile.name.toLowerCase() == "temp.txt") {
				title_string = "Temperature Data";
				yaxis_string = "Temperature (C)";
			}

			var layout = {
				title: {
					text: title_string,
					font: {
						family: 'Courier',
						size: 18
					},
				},
				xaxis: {
					title: {
						text: xaxis_string,
						font: {
							family: 'Courier',
							size: 14
						},
					},
				},
				yaxis: {
					title: {
						text: yaxis_string,
						font: {
							family: 'Courier',
							size: 14
						},
					},
				}
			};

			Plotly.newPlot(chart, data, layout);

			/* Current Plotly.js version */
			console.log( Plotly.BUILD );

		} else {  // if the file type is not csv, alert the user.

			console.log("File input invalid.");

			alert("Cannot process " + inputFile.name + ". The file must be in csv format.");

		}
	}

	fileReader.readAsText(inputFile, "UTF-8");

	console.log(fileReader);

}

// Loading dummy data when the website first loads.
function loadDummyData() {

	chart = document.getElementById('chart');

	var input_data = {
		x: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
		y: [25, 24, 27, 29, 31, 27, 24, 20, 17, 19]
	}
	var data = [input_data];

	// Formatting Layout.
	// Title.
	var title_string = "Humidity Data",
		xaxis_string = "Time (s)",
		yaxis_string = "Humidity (%)";

	var layout = {
		title: {
			text: title_string,
			font: {
				family: 'Courier',
				size: 18
			},
		},
		xaxis: {
			title: {
				text: xaxis_string,
				font: {
					family: 'Courier',
					size: 14
				},
			},
		},
		yaxis: {
			title: {
				text: yaxis_string,
				font: {
					family: 'Courier',
					size: 14
				},
			},
		}
	};

	Plotly.newPlot(chart, data, layout);
}